const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const server = http.createServer(app);

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

server.listen(app.get('port'), () => {
    console.log('Server is running on port', app.get('port'));
});

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.post('/validar', (request, response) => {
    var jsonString = JSON.stringify(request.body);
    var data = JSON.parse(jsonString);
    var result = {};

    // Obtengo cada dato
    var cadena = data["cadena"];
    var estados = data["estados"];
    var estadoInicial = data["estadoInicial"];
    var estadoAceptacion = data["estadoAceptacion"];
    var transiciones = new Map(data["transiciones"]);

    var matrizAdyacencia = crearMatrizAdyacencia(estados, transiciones);
    var resultado = validarCadena(cadena, matrizAdyacencia, estadoAceptacion);

    result["cadena"] = data["cadena"];
    result["estado"] = resultado;

    //console.log(jsonString);
    //console.log(cadena);
    //console.log(estados);
    //console.log(estadoInicial);
    console.log(estadoAceptacion);
    //console.log(transiciones);
    console.log(matrizAdyacencia);
    //var transiciones2 = Array.from(transiciones.keys());
    //var transicion = transiciones2[1];
    //console.log(transiciones.get(transicion));
    //console.log(transiciones2);
    //console.log(transicion);
    //console.log(transicion[1]);

    response.send(JSON.stringify(result));
});

app.post('/crearTabla', (request, response) => {
    console.log('body: ' + JSON.stringify(request.body));
    response.send(request.body);
});

function crearMatrizAdyacencia(estados, transicionesMap) {
    // Array { '1', '2' }
    // Map { [ 1, 1 ] => 'A', [ 1, 2 ] => 'B' }
    var n = estados.length + 1;
    var matrizAdyacencia = new Array(n);

    // Crear matriz
    for (var i = 0; i < n; i++) {
        matrizAdyacencia[i] = new Array(n);
        for (var j = 0; j < n; j++) {
            matrizAdyacencia[i][j] = 0;
        }
    }
    
    // Guardar estados
    for (let index = 1; index < n; index++) {
        let index2 = index - 1;
        let estado = parseInt(estados[index2], 10);
        matrizAdyacencia[index][0] = estado;
        matrizAdyacencia[0][index] = estado;
    }

    // Guardar transiciones
    var x, y;
    var transiciones = Array.from(transicionesMap.keys());
    for (var index = 0; index < transiciones.length; index++) {
        var transicion = transiciones[index];
        var inicial = transicion[0];
        var final = transicion[1];
        var lexema = transicionesMap.get(transicion);

        for (var i = 1; i < n; i++) {
            if (matrizAdyacencia[i][0] === inicial)
                x = i;
            if (matrizAdyacencia[0][i] === final)
                y = i;
        }

        matrizAdyacencia[x][y] = lexema;
    }

    return matrizAdyacencia;
}

function validarCadena(cadena, matrizAdyacencia, estadoAceptacion) {
    var i = 1;
    var j = 1;
    var estadoFinal;

    for (var index = 0; index < cadena.length; index++) {
        var lexema = cadena[index];
        
        while (matrizAdyacencia[i][j] !== lexema) {
            j++;
        }
        estadoFinal = matrizAdyacencia[0][j];
        i = j;
        j = 1;
    }

    if (estadoFinal == estadoAceptacion)
        return true;

    return false;
}
 
module.exports = app;