const ACCEPTANCE = 1;
const NORMAL = 0;
const radius = 30;

var object = null;
var idState = 1;

// DATOS QUE SE ENVIAN AL SERVIDOR
var estados = new Set();
var transiciones = new Map();
var estadoAceptacion = 0;
var estadoInicial = 1;
// -------------------------------------------

var stage = new Konva.Stage({
    container: 'container',
    width: window.innerWidth, 
    height: window.innerHeight
});

var layer = new Konva.Layer();
stage.add(layer);


stage.on('click', function (e) {
    object = e.target;

    if (object.getClassName() === 'Circle') {
        //alert(object.getId());
    }

});

stage.on('dblclick', function (e) {
    object = e.target;

    // Crea un nuevo estado
    if (object === stage) {
        posX = object.getPointerPosition().x;
        posY = object.getPointerPosition().y;

        var group = new Konva.Group({
            draggable: false
        });

        var circle = new Konva.Circle({
            x: posX,
            y: posY,
            radius,
            fill: '#00D2FF',
            stroke: 'black',
            strokeWidth: 1,
            id: String(idState++),
            name: String(NORMAL)
        });

        var text = new Konva.Text({
            text: circle.id(),
            fontSize: 16,
            fill: 'black'
        });

        var label = new Konva.Label({
            x: -text.width() / 2 + circle.getX(),
            y: -text.height() / 2 + circle.getY()
        });

        label.listening('false');
        text.listening('false');
        circle.listening('inherit');

        label.add(text);
        group.add(circle, label);
        layer.add(group);
        layer.draw()

        // Agrega el estado creado a la lista de estados
        estados.add(circle.id());
    }
});

$("#btnCrearTransicion").click(function (e) {
    e.preventDefault();

    // DATOS PARA CREAR LA TRANSICION ENTRE ESTADOS
    var $lexema = $("#txtLexema").val();
    var $inicialState = $("#txtEstadoInicial").val();
    var $finalState = $("#txtEstadoFinal").val();

    drawTransaction($inicialState, $finalState, $lexema);

    var transicion = new Array(parseInt($inicialState, 10), parseInt($finalState, 10));
    transiciones.set(transicion, $lexema.toString());
    
});

$("#btnValidarCadena").click(function (e) {
    e.preventDefault();
    
    var $cadena = $('#txtCadena').val();
    var obj = {};

    obj["cadena"] = $cadena.toString();
    obj["estadoAceptacion"] = estadoAceptacion;
    obj["estadoInicial"] = estadoInicial;
    obj["estados"] = Array.from(estados);
    obj["transiciones"] = Array.from(transiciones);

    //alert(JSON.stringify(obj));

    $.ajax({
        url: 'http://localhost:3000/validar',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(obj),
        processData: false,
        success: function (data, textStatus, jQxhr) {
            obj = JSON.parse(data);
            if (obj["estado"] === true)
                alert("La cadena " + obj["cadena"] + " es correcta");
            else
                alert("La cadena " + obj["cadena"] + " es incorrecta");
        },
        error: function (jqXhr, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
});

function drawTransaction(inicio, fin, lexema) {
    var nodo1 = stage.findOne('#' + inicio);
    var nodo2 = stage.findOne('#' + fin);

    var arrow = new Konva.Arrow({
        points: [nodo1.getX() + radius, nodo1.getY(), nodo2.getX() - radius, nodo2.getY()],
        pointerLength: 10,
        pointerWidth: 10,
        fill: 'black',
        stroke: 'black',
        strokeWidth: 1
    });

    var text = new Konva.Text({
        text: lexema,
        fontSize: 16,
        fill: 'black'
    });

    var label = new Konva.Label({
        x: ((nodo1.getX() + nodo2.getX()) / 2),
        y: ((nodo1.getY() + nodo2.getY()) / 2)
    });

    label.listening('false');
    text.listening('false');

    label.add(text);
    layer.add(arrow, label);
    layer.batchDraw();
}

layer.on('click', function (e) {
    object = e.target;
});

// Creo estados de aceptacion
layer.on('dblclick', function (e) {
    object = e.target;
    //Establece un estado de aceptacion
    if (object.getClassName() === 'Circle') {
        switch (object.strokeWidth()) {
            case 1:
                object.strokeWidth(6);
                object.name(String(ACCEPTANCE)); //Aceptacion
                estadoAceptacion = object.id();
                break;
            default:
                object.strokeWidth(1);
                object.name(String(NORMAL)); //Normal
                estadoAceptacion = 0;
        }
    }
    layer.draw();
});

var container = stage.container();
container.tabIndex = 1;
container.focus();

// Elimino estados
container.addEventListener('keydown', function (e) {
    object = object.getParent();
    var obj = ((object.getClassName() === 'Label') ? object.getParent() : object);

    if (e.which === 46 || e.keyCode === 46) {
        obj.destroy();
    }

    e.preventDefault();
    layer.batchDraw();
});